# Danish translation pconsole.
# Copyright (C) pconsole & nedenstående oversættere.
# This file is distributed under the same license as the pconsole package.
# Joe Hansen (joedalton2@yahoo.dk), 2011.
#
msgid ""
msgstr ""
"Project-Id-Version: pconsole\n"
"Report-Msgid-Bugs-To: pconsole@packages.debian.org\n"
"POT-Creation-Date: 2008-05-06 19:30+0200\n"
"PO-Revision-Date: 2011-05-28 18:30+01:00\n"
"Last-Translator: Joe Hansen <joedalton2@yahoo.dk>\n"
"Language-Team: Danish <debian-l10n-danish@lists.debian.org>\n"
"Language: da\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "Do you want /usr/bin/pconsole to be installed SUID root?"
msgstr "Ønsker du at /usr/bin/pconsole skal installeres som SUID root?"

#. Type: boolean
#. Description
#: ../templates:1001
msgid "You are strongly encouraged to leave pconsole without the SUID bit on."
msgstr "Du anbefales stærkt at efterlade pconsole uden SUID-delen aktiveret."

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"If you are using a multiuser system, setting the SUID bit on pconsole will "
"be a major risk for a breach, since normal users will be able to attach "
"their consoles to a root PTY/TTY and send commands as root."
msgstr ""
"Hvis du bruger et flerbrugersystem, vil angivelse af SUID-delen på pconsole "
"være en alvorlig sikkerhedsrisiko, da normale brugere vil være i stand til "
"at vedhæfte deres konsoller til en root PTY/TTY og sende kommandoer som "
"administrator (root)."

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"However, on a single-user system, setting the SUID bit might be a good idea "
"to avoid logging as root user."
msgstr ""
"På et system for en enkelt bruger kan angivelse af SUID dog være en god ide "
"for at undgå logind som administratorbruger."

#. Type: boolean
#. Description
#: ../templates:1001
msgid ""
"If in doubt, you should install it without SUID. If it causes problems you "
"can change your mind later by running: dpkg-reconfigure pconsole"
msgstr ""
"Hvis du er i tvivl, skal du installere den uden SUID. Hvis det giver "
"problemer kan du skifte mening senere ved at køre: dpkg-reconfigure pconsole."
